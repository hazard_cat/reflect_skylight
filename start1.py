# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 05:07:49 2014

@author: kshmirko
"""

from tools.sky_radiance import polarization_sky
from numpy import array, pi

a,b = polarization_sky(pi/4, 0, array([pi/4]), array([pi]), 0)
print(a.shape,b)