# -*- coding: utf-8 -*-
"""
Created on Sat Nov 15 22:58:28 2014

@author: kshmirko
"""

from numpy import asmatrix, mat, asarray, sqrt

__all__=['norm_v', 'norm_v3','dot_product']


def norm_v(V):
  """
  calculater norms of vectors V
  :V: array of 3d vectors Nx3
  
  Returns 
  array Nx3 where every row is norm(V[i,:])
  """
  
  V = sqrt(asmatrix(V**2).sum(1))
  ONE = mat([1.,1.,1.])
  N = V*ONE
  return asarray(N)
  
  
def dot_product(a,b):
  """
  Скалярное произведение векторов вида 1x3, Nx3, MxNx3
  """
  n = len(a.shape)-1
  ret=(a*b).sum(n)
  return ret


def norm_v3(V):
  """
  Вычисляет норму векторов в 3-х мерном пространстве.
  :V: массив 3-D векторов NxMx3 
  """
  
  ret = sqrt(dot_product(V,V))
  return ret


if __name__=='__main__':
  print(norm_v(asarray([[1,2,3],[2,3,4]])))