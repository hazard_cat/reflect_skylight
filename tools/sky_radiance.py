# -*- coding: utf-8 -*-
"""
Created on Sat Nov 15 16:12:06 2014

@author: kshmirko

Provides following functions:
- sky_radiance
- sky_radiance_clear
- sky_radiance_opaque

"""
import numpy as np
from numpy import arccos, cos, sin, exp, pi, sqrt
from .coords import sph_2_cart,  cart_2_sph
from .norms import norm_v3, dot_product

__all__=['sky_radiance',
         'sky_radiance_clear',
         'sky_radiance_opaque'
         ]


def sky_radiance_clear(phi_v, theta_v, theta_s):
  """
  Calculates sky radiance in clearsky conditions
  :phi_v: - skypoint azimuth, radians
  :theta_v: - skypoint zenith, radians
  :theta_s: - solar zenith, radians
  """
  
  psi = arccos(cos(theta_v)*cos(theta_s)+sin(theta_v)*sin(theta_s)*sin(phi_v))
  
  Nc = (1.63+53.7*exp(-5.49*psi)+2.04*cos(psi)**2*cos(theta_s))*\
        (1-exp(-0.19/cos(theta_v)))*(1-exp(-0.53/cos(theta_s)))
  
  return Nc

  
def sky_radiance_opaque(phi_v, theta_v, theta_s):
  """
  Calculates sky radiance in cloudy conditions
  :phi_v: - skypoint azimuth, radians
  :theta_v: - skypoint zenith, radians
  :theta_s: - solar zenith, radians
  """
  
  psi = arccos(cos(theta_v)*cos(theta_s)+sin(theta_v)*sin(theta_s)*sin(phi_v))
  
  No = 0.45+0.12*theta_s+0.43*cos(theta_v)+0.72*exp(-1.88*psi);
  
  return No
  
  
def sky_radiance(phi_v, theta_v, phi_s, theta_s, Cloud):
  """
  Calculates sky radiance for given cloudy conditions.
  :phi_v: - skypoint azimuth, radians
  :theta_v: - skypoint zenith, radians
  :phi_s: - solar azimuth, radians
  :theta_s: - solar zenith, radians
  :Cloud: - cloud conditions, 0..1
  """
  
  phi_v = -phi_v-phi_s+np.pi/2
  N = Cloud*sky_radiance_opaque(phi_v,theta_v, theta_s)+\
      (1.0-Cloud)*sky_radiance_clear(phi_v, theta_v, theta_s)
      
  return N
  
  
def polarization_sky(theta_s, phi_s, theta_v, phi_v, cloud):
  """
  Вычисляет вектор стокса и степень линейной поляризации неба в рэлеевском 
  приближении.
  """
  
  phi_v = 2*pi-phi_v+pi/2.0
  
  N = sky_radiance(phi_v, theta_v, phi_s, theta_s, cloud)
  
  psi = phi_v - phi_s
  
  cos_gamma = sin(theta_v)*sin(theta_s)*cos(psi)+cos(theta_s)*cos(theta_v)
  
  Pol_ratio = (sin(arccos(cos_gamma))**2)/(1+cos_gamma**2)
  
  L = Pol_ratio*N
  
  W = (1-Pol_ratio)*N
  
  vx, vy, vz = sph_2_cart(phi_v, theta_v)
  
  V = np.dstack((vx,vy,vz))
  
  sx, sy, sz = sph_2_cart(phi_s, theta_s)
  
  S = np.zeros(V.shape)
  S[:,:,0] = sx
  S[:,:,1] = sy
  S[:,:,2] = sz
  
  
  Norm = np.zeros(V.shape)
  Norm[:,:,2] = 1
  
  nv = np.cross(V, Norm)
  ns = np.cross(V,S)
  
  cosw = dot_product(nv,ns)/(norm_v3(nv)*norm_v3(ns))
  cosw2 = cosw*cosw
  sinw2 = 1-cosw2
  
  Ip = L*cosw2+W/2.0
  Is = L*sinw2+W/2.0
  
  cos2w = (Ip-Is)/L
  sin2w = sqrt(1-cos2w**2)
  
  S0 = Ip+Is
  S1 = Ip-Is
  S2 = L*sin2w
  S3 = np.zeros(S2.shape)
  
  Stokes = np.dstack((S0,S1,S2,S3))
  
  return Stokes, Pol_ratio
  
  
if __name__ == '__main__':
  a,b = polarization_sky(pi/4, 0, [pi/4], [0], 0)
  print(a,b)