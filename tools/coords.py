# -*- coding: utf-8 -*-
"""
Created on Sat Nov 15 15:40:39 2014

@author: kshmirko

Provides following functions:
- sph_2_cart
- cart_2_sph

"""

import numpy as np

__all__ = ['sph_2_cart','cart_2_sph']


def sph_2_cart(phi, theta):
  """
  Transphorms spherical coordinates to cartesian ones.
  
  :phi: azimuth, clockwise from x to y, radians 
  :theta: zenith, from z to xy plane, radians
  
  Returns
  X, Y, Z

     z
     \
     \
     \
     --------------- y
    /
   /
  / 
 /  
x
  """
  Z = np.cos(theta)
  X = np.sin(theta)*np.cos(phi)
  Y = np.sin(theta)*np.sin(phi)
  
  
  return X,Y,Z
  
  
def cart_2_sph(X,Y,Z):
  """
  Transphorms cartesian coordinates to spherical ones.
  
  :X: x - coordinates
  :Y: y - coordinates
  :Z: z - coordinates
  
  Returns
  phi, theta, r
  """
  
  hypotxy = np.hypot(X,Y)
  r = np.hypot(hypotxy,Z)
  theta = np.arccos(Z/r)
  phi = np.arctan2(Y,X)
  
  return phi, theta, r
  