# -*- coding: utf-8 -*-
"""
Created on Sat Nov 15 22:23:45 2014

@author: kshmirko
"""
from numpy import sin, arcsin, tan, zeros, sqrt

__all__ = ['fresnel3']

def fresnel3(theta1, m1, m2):
  """
  Calculates fresnel coefficients
  :theta1: zenith of incident ray, radians
  :m1: media refractive index
  :m2: matter refractive index
  
  Returns
  - rs
  - rp
  - R - reflection matrix
  """
  
  sin_theta2=sin(theta1)*m1/m2
  theta2 = arcsin(sin_theta2)
  
  rs = sqrt((sin(theta1-theta2)**2)/(sin(theta1+theta2)**2))
  rp = sqrt((tan(theta1-theta2)**2)/(tan(theta1+theta2)**2))

  R = zeros((len(theta1),4,4))
  R[:,0,0]=rp**2+rs**2
  R[:,0,1]=rp**2-rs**2
  R[:,1,0]=R[:,0,1]
  R[:,1,1]=R[:,0,0]
  R[:,2,2]=-2*rp*rs
  R[:,3,3]=R[:,2,2]
  R=0.5*R
  
  return rp, rs, R
