# -*- coding: utf-8 -*-
"""
Created on Sat Nov 15 16:49:34 2014

@author: kshmirko

Gives the slope proability

Provides following functions:

- simple_prob
- adv_prob
"""

from numpy import hypot, cos, sin, sqrt, exp, pi


__all__=['simple_prob',
         'adv_prob']


def adv_prob(wspd, Zx, Zy, phw, slick):
  """
  :wspd: - wind speed, m/s
  :Zx: - x slope component
  :Zy: - y slope component
  :phw: - relative azimuth, radians
  :slick: - true - slick surface
  """
  
  tantilt = hypot(Zx,Zy)
  
  if slick:
    sigmaC=0.003+0.00084*wspd
    sigmaU=0.005+0.00078*wspd
    C21=0
    C03=0.02
    C40=0.36
    C22=0.10
    C04=0.26
  else:
    sigmaC=0.003+0.00192*wspd
    sigmaU=0.00316*wspd
    C21=0.01-0.0086*wspd
    C03=0.04-0.033*wspd
    C40=0.40
    C22=0.12
    C04=0.23
  
  xe=(cos(phw)*Zx+sin(phw)*Zy)/sqrt(sigmaC)
  xn=(-sin(phw)*Zx+cos(phw)*Zy)/sqrt(sigmaU)
  xe2=xe*xe
  xn2=xn*xn
  coef=1-C21/2.0*(xe2-1)*xn-C03/6.0*(xn2-3)*xn
  coef=coef+C40/24.0*(xe2*xe2-6*xe2+3)
  coef=coef+C04/24.0*(xn2*xn2-6*xn2+3)
  coef=coef+C22/4.0*(xe2-1)*(xn2-1)
  proba=coef*exp(-(xe2+xn2)/2.0)/(2.0*pi*sqrt(sigmaU)*sqrt(sigmaC))
  
  return proba
  

def simple_prob(wspd, Zx, Zy):
  """
  :wspd: - wind speed, m/s
  :Zx: - x slope component
  :Zy: - y slope component
  """
  
  s2 = 0.003+0.00512*wspd
  p = 1.0/(pi*s2)
  p=p.*exp(-(zx.^2+zy.^2)/s2)

  return p