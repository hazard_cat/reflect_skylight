# -*- coding: utf-8 -*-
"""
Created on Sat Nov 15 15:17:54 2014

@author: kshmirko
"""
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import pylab as plt

from tools.coords import sph_2_cart, cart_2_sph
from tools.sky_radiance import sky_radiance
from tools.reflectance import fresnel3


N = 100
phi = np.linspace(0,2*np.pi,N)
theta = np.linspace(0, np.pi/2, N)

PHI, THETA = np.meshgrid(phi, theta)

phi_s = 0
theta_s = np.pi/3
Nc = sky_radiance(PHI, THETA, phi_s, theta_s, .5)
 
X,Y,Z = sph_2_cart(PHI,THETA)

rp, rs, R = fresnel3([np.pi/4, np.pi/6], 1, 4/3.0)
 
fig = plt.figure()
ax = fig.add_subplot(111,projection='polar')

#ax.contourf(PHI, THETA, Nc)
cset1 = ax.contourf(PHI, np.sin(THETA), Nc, 10, linestyle='--', origin='upper', alpha=0.6)
plt.colorbar(cset1)
ax.contour(PHI, np.sin(THETA), Nc, cset1.levels, colors='k')
plt.show()
